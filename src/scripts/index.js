import '../styles/index.scss';
import {
    Clock,
    DirectionalLight,
    Mesh,
    MeshStandardMaterial,
    PerspectiveCamera,
    PlaneBufferGeometry,
    PointLight,
    Scene,
    TextureLoader,
    WebGLRenderer
} from 'three';

class Portal {

    constructor() {
        this.scene = null;
        this.sceneLight = null;
        this.portalLight = null;
        this.cam = null;
        this.renderer = null;
        this.clock = null;
        this.portalParticles = [];
        this.smokeParticles = [];
    }

    initScene() {
        this.scene = new Scene();

        this.sceneLight = new DirectionalLight(0xffffff, 0.5);
        this.sceneLight.position.set(0, 0, 1);
        this.scene.add(this.sceneLight);

        this.portalLight = new PointLight(0x062d89, 30, 600, 1.7);
        this.portalLight.position.set(0, 0, 250);
        this.scene.add(this.portalLight);

        this.cam = new PerspectiveCamera(80, window.innerWidth / window.innerHeight, 1, 10000);
        this.cam.position.z = 1000;
        this.scene.add(this.cam);

        this.renderer = new WebGLRenderer();
        this.renderer.setClearColor(0x000000, 1);
        this.renderer.setSize(window.innerWidth, window.innerHeight);

        document.body.appendChild(this.renderer.domElement);

        this._particleSetup();
    }

    _particleSetup() {
        new TextureLoader().load("public/smoke.png", (texture) => {
            const portalGeo = new PlaneBufferGeometry(350, 350);
            const portalMaterial = new MeshStandardMaterial({
                map: texture,
                transparent: true
            });
            const smokeGeo = new PlaneBufferGeometry(1000, 1000);
            const smokeMaterial = new MeshStandardMaterial({
                map: texture,
                transparent: true
            });

            for (let p = 880; p > 250; p--) {
                const particle = new Mesh(portalGeo, portalMaterial);
                particle.position.set(
                    0.5 * p * Math.cos((4 * p * Math.PI) / 180),
                    0.5 * p * Math.sin((4 * p * Math.PI) / 180),
                    0.1 * p
                );
                particle.rotation.z = Math.random() * 360;
                this.portalParticles.push(particle);
                this.scene.add(particle);
            }

            for (let p = 0; p < 40; p++) {
                const particle = new Mesh(smokeGeo, smokeMaterial);
                particle.position.set(
                    Math.random() * 1000 - 500,
                    Math.random() * 400 - 200,
                    25
                );
                particle.rotation.z = Math.random() * 360;
                particle.material.opacity = 0.6;
                this.portalParticles.push(particle);
                this.scene.add(particle);
            }

            this.clock = new Clock();
            this._animate();
        });
    }

    _animate() {
        const delta = this.clock.getDelta();

        this.portalParticles.forEach(p => {
            p.rotation.z -= delta * 1.5;
        });

        this.smokeParticles.forEach(p => {
            p.rotation.z -= delta * 0.2;
        });

        if (Math.random() > 0.9) {
            this.portalLight.power = 350 + Math.random() * 500;
        }

        this.renderer.render(this.scene, this.cam);
        requestAnimationFrame(this._animate.bind(this));
    }

}

new Portal().initScene();